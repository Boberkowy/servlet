package Database;


import com.sun.org.apache.bcel.internal.generic.RETURN;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Person {

	 private String name;
	 private String surname;
	 private String email;
	 private String employer;
	 private String getKnow;
	 private String hobbies;


	public String getSurname(HttpServletRequest request) {
		return  request.getParameter("surname");
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName(HttpServletRequest request) {
		return request.getParameter("name");
	}

	public void setName(String name) {
		this.name = name;

	}

	public String getEmail(HttpServletRequest request) {
		return request.getParameter("email");
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployer(HttpServletRequest request) {
		return request.getParameter("pracodawca");
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getGetKnow(HttpServletRequest request) {
		return request.getParameter("know");
	}

	public void setGetKnow(String getKnow) {
		this.getKnow = getKnow;
	}

	public String getHobbies(HttpServletRequest request) {
		return request.getParameter("hobbies");
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}





}
