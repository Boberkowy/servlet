package Database;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Boberkowy on 2015-12-13.
 */
public class dbConnection {

    private static Connection conn = null;

    public static Connection getConnection() {
        Connection conn = null;
        try{
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/formularz", "postgres",
                    "qwerty");
            return conn;

        }catch(Exception e){
            e.printStackTrace();
        }
        System.out.println("Connected to database");
        return conn;
    }

    public static void closeConnection(){
        try {
            conn.close();
            conn=null;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
