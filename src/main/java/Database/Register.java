package Database;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Register")
public class Register extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		dbConnection connection = new dbConnection();
		Connection conn = null;
		Person person = new Person();
		String name = person.getName(request);
		String surname=person.getSurname(request);
		String email= person.getEmail(request);
		String nameC=person.getEmployer(request);
		String getKnow = person.getGetKnow(request);
		String hobbies = person.getHobbies(request);
		HttpSession session = request.getSession();

		try{
			if(conn==null){

				conn = connection.getConnection();
			}

			if(checkCount() >= 5){
				response.sendRedirect("brakmiejsc.html");
			}

			else {
				session.setAttribute("zalogowany", "added");
				PreparedStatement ps = conn.prepareStatement(
						"INSERT INTO zgloszenia(name,surname, email,pracodawca, getknow, hobbies) values(?,?,?,?,?,?)");
				ps.setString(1, name);
				ps.setString(2, surname);
				ps.setString(3, email);
				ps.setString(4, nameC);
				ps.setString(5, getKnow);
				ps.setString(6, hobbies);

				int i = ps.executeUpdate();
				if (i > 0) {
					String per = (String)session.getAttribute("name");
					System.out.println( per);
					response.sendRedirect("done.html");
						connection.closeConnection();
				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		out.close();
	}


	public int checkCount(){
		dbConnection connection = new dbConnection();
		Connection conn = connection.getConnection();
		if(conn == null){
			conn = connection.getConnection();
		}
		Statement stmt = null;
		int count = 0;
		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM zgloszenia;";
		ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				count++;
				System.out.println(count);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return count;
	}
}


